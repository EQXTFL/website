Linux version 2.2.16 (root@linux) (gcc version 2.95.2 19991024 (release)) #3 Fri Sep 15 09:19:28 CEST 2000
Detected 467743 kHz processor.
ide_setup: hdd=ide-scsi
Console: colour VGA+ 80x25
Calibrating delay loop... 933.89 BogoMIPS
Memory: 257476k/262080k available (1232k kernel code, 412k reserved, 2884k data, 76k init, 0k bigmem)
Dentry hash table entries: 32768 (order 6, 256k)
Buffer cache hash table entries: 262144 (order 8, 1024k)
Page cache hash table entries: 65536 (order 6, 256k)
CPU: Intel Celeron (Mendocino) stepping 05
Checking 386/387 coupling... OK, FPU using exception 16 error reporting.
Checking 'hlt' instruction... OK.
POSIX conformance testing by UNIFIX
mtrr: v1.35a (19990819) Richard Gooch (rgooch@atnf.csiro.au)
PCI: PCI BIOS revision 2.10 entry at 0xfb290, last bus=1
PCI: Using configuration type 1
PCI: Probing PCI hardware
PCI: 00:38 [1106/0596]: Work around ISA DMA hangs (00)
Activating ISA DMA hang workarounds.
Linux NET4.0 for Linux 2.2
Based upon Swansea University Computer Society NET3.039
NET4: Unix domain sockets 1.0 for Linux NET4.0.
NET4: Linux TCP/IP 1.0 for NET4.0
IP Protocols: ICMP, UDP, TCP
TCP: Hash tables configured (ehash 262144 bhash 65536)
Initializing RT netlink socket
Starting kswapd v 1.5 
parport0: PC-style at 0x378 (0x778) [SPP,ECP,ECPEPP,ECPPS2]
parport0: detected irq 7; use procfs to enable interrupt-driven operation.
matroxfb: Matrox MGA-G200 (AGP) detected
matroxfb: MTRR's turned on
matroxfb: 640x480x8bpp (virtual: 640x26208)
matroxfb: framebuffer at 0xD8000000, mapped to 0xd0005000, size 16777216
Console: switching to colour frame buffer device 80x30
fb0: MATROX VGA frame buffer device
Detected PS/2 Mouse Port.
pty: 256 Unix98 ptys configured
apm: BIOS version 1.2 Flags 0x07 (Driver version 1.13)
Real Time Clock Driver v1.09
Uniform Multi-Platform E-IDE driver Revision: 6.30
ide: Assuming 33MHz system bus speed for PIO modes; override with idebus=xx
VP_IDE: IDE controller on PCI bus 00 dev 39
VP_IDE: chipset revision 16
VP_IDE: not 100% native mode: will probe irqs later
VT 82C691 Apollo Pro
 Chipset Core ATA-66
Split FIFO Configuration:  8 Primary buffers, threshold = 1/2
                           8 Second. buffers, threshold = 1/2
    ide0: BM-DMA at 0x9000-0x9007, BIOS settings: hda:DMA, hdb:DMA
ide0: VIA Bus-Master (U)DMA Timing Config Success
    ide1: BM-DMA at 0x9008-0x900f, BIOS settings: hdc:DMA, hdd:DMA
ide1: VIA Bus-Master (U)DMA Timing Config Success
HPT366: IDE controller on PCI bus 00 dev 80
HPT366: chipset revision 1
HPT366: not 100% native mode: will probe irqs later
    ide2: BM-DMA at 0xa000-0xa007, BIOS settings: hde:DMA, hdf:DMA
HPT366: IDE controller on PCI bus 00 dev 81
HPT366: chipset revision 1
HPT366: not 100% native mode: will probe irqs later
    ide3: BM-DMA at 0xac00-0xac07, BIOS settings: hdg:pio, hdh:pio
hda: IBM-DJNA-352030, ATA DISK drive
hdb: QUANTUM FIREBALLlct10 15, ATA DISK drive
hdc: _NEC DV-5700A, ATAPI CDROM drive
hdd: 4X4X32, ATAPI CDROM drive
hde: QUANTUM FIREBALLlct10 15, ATA DISK drive
hdf: WDC AC28400R, ATA DISK drive
ide0 at 0x1f0-0x1f7,0x3f6 on irq 14
ide1 at 0x170-0x177,0x376 on irq 15
ide2 at 0x9800-0x9807,0x9c02 on irq 11
hda: IBM-DJNA-352030, 19470MB w/1966kB Cache, CHS=2482/255/63
hdb: QUANTUM FIREBALLlct10 15, 14324MB w/418kB Cache, CHS=1826/255/63
hde: QUANTUM FIREBALLlct10 15, 14324MB w/418kB Cache, CHS=29104/16/63, UDMA(33)
hdf: WDC AC28400R, 8063MB w/512kB Cache, CHS=16383/16/63, UDMA(33)
hdc: ATAPI 40X DVD-ROM drive, 256kB Cache, UDMA(33)
Uniform CD-ROM driver Revision: 3.11
Floppy drive(s): fd0 is 1.44M
FDC 0 is a post-1991 82077
aec671x_detect: 
   ACARD AEC-671X PCI Ultra/W SCSI-3 Host Adapter: 0    IO:c800, IRQ:11.
         ID:  2  Color   FlatbedScanner180060
         ID:  7  Host Adapter
ppa: Version 2.03 (for Linux 2.2.x)
ppa: Found device at ID 6, Attempting to use EPP 32 bit
ppa: Communication established with ID 6 using EPP 32 bit
scsi0 : ACARD AEC-6710/6712 PCI Ultra/W SCSI-3 Adapter Driver V2.0+ac 
scsi1 : SCSI host adapter emulation for IDE ATAPI devices
scsi2 : Iomega VPI0 (ppa) interface
scsi : 3 hosts.
  Vendor: Color     Model: FlatbedScanner18  Rev: 0060
  Type:   Scanner                            ANSI SCSI revision: 02
Detected scsi generic sg0 at scsi0, channel 0, id 2, lun 0
  Vendor: ATAPI     Model: CD-R/RW 4X4X32    Rev: 3.MR
  Type:   CD-ROM                             ANSI SCSI revision: 02
Detected scsi CD-ROM sr0 at scsi1, channel 0, id 0, lun 0
  Vendor: IOMEGA    Model: ZIP 100           Rev: D.06
  Type:   Direct-Access                      ANSI SCSI revision: 02
scsi : detected 3 SCSI generics 1 SCSI cdrom total.
sr0: scsi3-mmc drive: 32x/32x writer cd/rw xa/form2 cdda tray
Partition check:
 hda: hda1 hda2 < hda5 hda6 hda7 hda8 hda9 >
 hdb: hdb1 hdb2 hdb3 < hdb5 hdb6 >
 hde: [PTBL] [1027/255/63] hde1 hde2 hde3 < hde5 hde6 hde7 hde8 hde9 >
 hdf: [PTBL] [1027/255/63] hdf1 hdf2 hdf3 < hdf5 hdf6 hdf7 hdf8 >
NTFS version 000607
udf: registering filesystem
VFS: Mounted root (ext2 filesystem) readonly.
Freeing unused kernel memory: 76k freed
Adding Swap: 130748k swap-space (priority -1)
Adding Swap: 265064k swap-space (priority -2)
Adding Swap: 265064k swap-space (priority -3)
Serial driver version 4.27 with SHARE_IRQ DETECT_IRQ enabled
ttyS00 at 0x03f8 (irq = 4) is a 16550A
ttyS01 at 0x02f8 (irq = 3) is a 16550A
/dev/vmmon: Module vmmon: registered with major=10 minor=165 tag=$Name: build-637 $
/dev/vmmon: Module vmmon: initialized
/dev/vmnet: open called by PID 349 (vmnet-bridge)
/dev/vmnet: hub 0 does not exist, allocating memory.
/dev/vmnet: port on hub 0 successfully opened
bridge-eth0: peer interface eth0 not found, will wait for it to come up
bridge-eth0: attached
/dev/vmnet: open called by PID 360 (vmnet-netifup)
/dev/vmnet: hub 1 does not exist, allocating memory.
/dev/vmnet: port on hub 1 successfully opened
/dev/vmnet: open called by PID 378 (vmnet-dhcpd)
/dev/vmnet: port on hub 1 successfully opened

